package edu.uni.poo.citamedic;


import edu.uni.poo.citamedic.User.Dao;
import edu.uni.poo.citamedic.User.Userimp;

public abstract class sdao{
        private static Dao userDao = null;

        public static Dao getDao(){
            if(userDao == null){
                userDao = new Userimp();
            }
            return userDao;
        }
}
